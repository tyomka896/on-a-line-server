pub use sea_orm_migration::prelude::*;

mod migrations;

use migrations::*;

pub struct Migrator;

#[async_trait::async_trait]
impl MigratorTrait for Migrator {
    fn migrations() -> Vec<Box<dyn MigrationTrait>> {
        vec![
            Box::new(m01_01_create_user_table::Migration),
            Box::new(m02_01_create_token_table::Migration),
            Box::new(m03_01_create_verify_table::Migration),
            Box::new(m04_01_create_chat_table::Migration),
            Box::new(m05_01_create_user_has_chat_table::Migration),
            Box::new(m06_01_create_message_table::Migration),
            Box::new(m06_02_create_message_trigger::Migration),
            Box::new(m07_01_create_participant_table::Migration),
            Box::new(m01_99_seed_user_table::Migration),
            Box::new(m04_99_seed_chat_table::Migration),
            Box::new(m05_99_seed_user_has_chat_table::Migration),
            Box::new(m06_99_seed_message_table::Migration),
        ]
    }
}
