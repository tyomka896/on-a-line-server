use sea_orm_migration::prelude::*;

use super::m01_01_create_user_table::Model as User;
use super::m04_01_create_chat_table::Model as Chat;
use super::m06_01_create_message_table::Model as Message;

#[derive(DeriveIden)]
pub enum Model {
    #[sea_orm(iden = "participant")]
    Table,
    ChatId,
    MessageId,
    UserId,
    IsRead,
    CreatedAt,
    UpdatedAt,
    DeletedAt,
}

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let table: TableCreateStatement = Table::create()
            .table(Model::Table)
            .if_not_exists()
            .col(ColumnDef::new(Model::ChatId).integer().not_null())
            .col(ColumnDef::new(Model::MessageId).integer().not_null())
            .col(ColumnDef::new(Model::UserId).integer().not_null())
            .col(
                ColumnDef::new(Model::IsRead)
                    .boolean()
                    .not_null()
                    .default(false),
            )
            .col(
                ColumnDef::new(Model::CreatedAt)
                    .timestamp_with_time_zone()
                    .not_null()
                    .default(Expr::current_timestamp()),
            )
            .col(
                ColumnDef::new(Model::UpdatedAt)
                    .timestamp_with_time_zone()
                    .not_null()
                    .default(Expr::current_timestamp()),
            )
            .col(ColumnDef::new(Model::DeletedAt).timestamp_with_time_zone())
            .primary_key(
                Index::create()
                    .name("participant_pkey")
                    .col(Model::ChatId)
                    .col(Model::MessageId)
                    .col(Model::UserId)
                    .primary(),
            )
            .foreign_key(
                ForeignKey::create()
                    .name("participant_chat_id_fkey")
                    .from(Model::Table, Model::ChatId)
                    .to(Chat::Table, Chat::Id)
                    .on_delete(ForeignKeyAction::Cascade)
                    .on_update(ForeignKeyAction::Cascade),
            )
            .foreign_key(
                ForeignKeyCreateStatement::new()
                    .name("participant_chat_id_message_id_fkey")
                    .from_tbl(Model::Table)
                    .from_col(Model::ChatId)
                    .from_col(Model::MessageId)
                    .to_tbl(Message::Table)
                    .to_col(Message::ChatId)
                    .to_col(Message::MessageId)
                    .on_delete(ForeignKeyAction::Cascade)
                    .on_update(ForeignKeyAction::Cascade),
            )
            .foreign_key(
                ForeignKey::create()
                    .name("participant_user_id_fkey")
                    .from(Model::Table, Model::UserId)
                    .to(User::Table, User::Id)
                    .on_delete(ForeignKeyAction::Cascade)
                    .on_update(ForeignKeyAction::Cascade),
            )
            .clone();

        manager.create_table(table).await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let table: TableDropStatement = Table::drop().table(Model::Table).if_exists().clone();

        manager.drop_table(table).await
    }
}
