use sea_orm_migration::prelude::*;

use super::m01_01_create_user_table::Model as User;

#[derive(DeriveIden)]
pub enum Model {
    #[sea_orm(iden = "token")]
    Table,
    Id,
    UserId,
    Name,
    Token,
    LastUsedAt,
    ExpiresAt,
    CreatedAt,
    UpdatedAt,
}

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let table: TableCreateStatement = Table::create()
            .table(Model::Table)
            .if_not_exists()
            .col(
                ColumnDef::new(Model::Id)
                    .integer()
                    .auto_increment()
                    .primary_key(),
            )
            .col(ColumnDef::new(Model::UserId).integer().not_null())
            .col(
                ColumnDef::new(Model::Name)
                    .string_len(100)
                    .not_null()
                    .default("auth"),
            )
            .col(ColumnDef::new(Model::Token).string_len(64).not_null())
            .col(ColumnDef::new(Model::LastUsedAt).timestamp_with_time_zone())
            .col(ColumnDef::new(Model::ExpiresAt).timestamp_with_time_zone())
            .col(
                ColumnDef::new(Model::CreatedAt)
                    .timestamp_with_time_zone()
                    .not_null()
                    .default(Expr::current_timestamp()),
            )
            .col(
                ColumnDef::new(Model::UpdatedAt)
                    .timestamp_with_time_zone()
                    .not_null()
                    .default(Expr::current_timestamp()),
            )
            .foreign_key(
                ForeignKey::create()
                    .name("token_user_id_fkey")
                    .from(Model::Table, Model::UserId)
                    .to(User::Table, User::Id)
                    .on_delete(ForeignKeyAction::Cascade)
                    .on_update(ForeignKeyAction::Cascade),
            )
            .clone();

        manager.create_table(table).await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let table: TableDropStatement = Table::drop().table(Model::Table).if_exists().clone();

        manager.drop_table(table).await
    }
}
