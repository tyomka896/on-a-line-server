use sea_orm_migration::prelude::*;

#[derive(DeriveIden)]
pub enum Model {
    #[sea_orm(iden = "message")]
    Table,
    ChatId,
    UserId,
    TypeId,
    Text,
}

#[derive(DeriveIden)]
pub enum ModelType {
    #[sea_orm(iden = "message_type")]
    Table,
    Id,
    About,
}

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let insert: InsertStatement = Query::insert()
            .into_table(ModelType::Table)
            .columns([ModelType::Id, ModelType::About])
            .values_panic(["message".into(), "Message with text only".into()])
            .clone();

        manager.exec_stmt(insert).await?;

        let insert: InsertStatement = Query::insert()
            .into_table(Model::Table)
            .columns([Model::ChatId, Model::UserId, Model::TypeId, Model::Text])
            .values_panic([1.into(), 1.into(), "message".into(), "text".into()])
            .values_panic([1.into(), 2.into(), "message".into(), "another".into()])
            .clone();

        manager.exec_stmt(insert).await
    }

    async fn down(&self, _: &SchemaManager) -> Result<(), DbErr> {
        Ok(())
    }
}
