use sea_orm_migration::prelude::*;

#[derive(DeriveIden)]
pub enum Model {
    #[sea_orm(iden = "user")]
    Table,
    Id,
    Username,
    FirstName,
    LastName,
    Email,
    Password,
    VerifiedAt,
    CreatedAt,
    UpdatedAt,
    LastVisit,
}

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let table: TableCreateStatement = Table::create()
            .table(Model::Table)
            .if_not_exists()
            .col(
                ColumnDef::new(Model::Id)
                    .integer()
                    .auto_increment()
                    .primary_key(),
            )
            .col(
                ColumnDef::new(Model::Username)
                    .string_len(50)
                    .unique_key()
                    .not_null(),
            )
            .col(ColumnDef::new(Model::FirstName).string_len(100))
            .col(ColumnDef::new(Model::LastName).string_len(100))
            .col(ColumnDef::new(Model::Email).string_len(100))
            .col(ColumnDef::new(Model::Password).string_len(60).not_null())
            .col(ColumnDef::new(Model::VerifiedAt).timestamp_with_time_zone())
            .col(
                ColumnDef::new(Model::CreatedAt)
                    .timestamp_with_time_zone()
                    .not_null()
                    .default(Expr::current_timestamp()),
            )
            .col(
                ColumnDef::new(Model::UpdatedAt)
                    .timestamp_with_time_zone()
                    .not_null()
                    .default(Expr::current_timestamp()),
            )
            .col(
                ColumnDef::new(Model::LastVisit)
                    .timestamp_with_time_zone()
                    .not_null()
                    .default(Expr::current_timestamp()),
            )
            .clone();

        manager.create_table(table).await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let table: TableDropStatement = Table::drop().table(Model::Table).if_exists().clone();

        manager.drop_table(table).await
    }
}
