use sea_orm_migration::prelude::*;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let db = manager.get_connection();

        db.execute_unprepared(
            "CREATE OR REPLACE FUNCTION public.f_message_before_insert()
                RETURNS trigger
                LANGUAGE plpgsql
            AS $function$
            DECLARE
                MESSAGE_ID_VALUE INT4;
            BEGIN
                SELECT max(message_id) INTO MESSAGE_ID_VALUE
                FROM message
                WHERE chat_id = NEW.chat_id;

                NEW.message_id = COALESCE(MESSAGE_ID_VALUE + 1, 1);

                RETURN NEW;
            END;
            $function$;",
        )
        .await?;

        db.execute_unprepared(
            "create trigger t_message_before_insert before
            insert on public.message for each row execute function f_message_before_insert()",
        )
        .await?;

        Ok(())
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let db = manager.get_connection();

        db.execute_unprepared("DROP TRIGGER t_message_before_insert ON public.message")
            .await?;

        db.execute_unprepared("DROP FUNCTION public.f_message_before_insert()")
            .await?;

        Ok(())
    }
}
