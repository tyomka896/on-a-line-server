use sea_orm_migration::prelude::*;

#[derive(DeriveIden)]
pub enum Model {
    #[sea_orm(iden = "user_has_chat")]
    Table,
    UserId,
    ChatId,
    TypeId,
}

#[derive(DeriveIden)]
pub enum ModelType {
    #[sea_orm(iden = "user_has_chat_type")]
    Table,
    Id,
    About,
}

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let insert: InsertStatement = Query::insert()
            .into_table(ModelType::Table)
            .columns([ModelType::Id, ModelType::About])
            .values_panic(["active".into(), "Active chat usage".into()])
            .values_panic(["banned".into(), "Banned in current chat".into()])
            .clone();

        manager.exec_stmt(insert).await?;

        let insert: InsertStatement = Query::insert()
            .into_table(Model::Table)
            .columns([Model::UserId, Model::ChatId, Model::TypeId])
            .values_panic([1.into(), 1.into(), "active".into()])
            .values_panic([2.into(), 1.into(), "active".into()])
            .clone();

        manager.exec_stmt(insert).await
    }

    async fn down(&self, _: &SchemaManager) -> Result<(), DbErr> {
        Ok(())
    }
}
