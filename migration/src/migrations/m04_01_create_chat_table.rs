use sea_orm_migration::prelude::*;

#[derive(DeriveIden)]
pub enum Model {
    #[sea_orm(iden = "chat")]
    Table,
    Id,
    Name,
    IsPrivate,
    CreatedAt,
    UpdatedAt,
}

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let table: TableCreateStatement = Table::create()
            .table(Model::Table)
            .if_not_exists()
            .col(
                ColumnDef::new(Model::Id)
                    .integer()
                    .auto_increment()
                    .primary_key(),
            )
            .col(ColumnDef::new(Model::Name).string_len(100))
            .col(ColumnDef::new(Model::IsPrivate).boolean().not_null())
            .col(
                ColumnDef::new(Model::CreatedAt)
                    .timestamp_with_time_zone()
                    .not_null()
                    .default(Expr::current_timestamp()),
            )
            .col(
                ColumnDef::new(Model::UpdatedAt)
                    .timestamp_with_time_zone()
                    .not_null()
                    .default(Expr::current_timestamp()),
            )
            .clone();

        manager.create_table(table).await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let table: TableDropStatement = Table::drop().table(Model::Table).if_exists().clone();

        manager.drop_table(table).await
    }
}
