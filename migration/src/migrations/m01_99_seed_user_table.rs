use sea_orm_migration::prelude::*;

#[derive(DeriveIden)]
pub enum Model {
    #[sea_orm(iden = "user")]
    Table,
    Username,
    Password,
}

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let password: &str = "$2b$12$APfmtd1xhUGKblcmaLwra.R.TGLx9b9PHMvCEXJW8wG05p34cZz0i";

        let insert: InsertStatement = Query::insert()
            .into_table(Model::Table)
            .columns([Model::Username, Model::Password])
            .values_panic(["user".into(), password.into()])
            .values_panic(["login".into(), password.into()])
            .clone();

        manager.exec_stmt(insert).await
    }

    async fn down(&self, _: &SchemaManager) -> Result<(), DbErr> {
        Ok(())
    }
}
