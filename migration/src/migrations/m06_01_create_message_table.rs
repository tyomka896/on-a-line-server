use sea_orm_migration::prelude::*;

use super::m01_01_create_user_table::Model as User;
use super::m04_01_create_chat_table::Model as Chat;

#[derive(DeriveIden)]
pub enum Model {
    #[sea_orm(iden = "message")]
    Table,
    ChatId,
    MessageId,
    UserId,
    TypeId,
    Text,
    CreatedAt,
    UpdatedAt,
}

#[derive(DeriveIden)]
pub enum ModelType {
    #[sea_orm(iden = "message_type")]
    Table,
    Id,
    About,
    CreatedAt,
}

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let table: TableCreateStatement = Table::create()
            .table(ModelType::Table)
            .if_not_exists()
            .col(ColumnDef::new(ModelType::Id).string_len(50).primary_key())
            .col(ColumnDef::new(ModelType::About).string_len(100))
            .col(
                ColumnDef::new(ModelType::CreatedAt)
                    .timestamp_with_time_zone()
                    .not_null()
                    .default(Expr::current_timestamp()),
            )
            .clone();

        manager.create_table(table).await?;

        let table: TableCreateStatement = Table::create()
            .table(Model::Table)
            .if_not_exists()
            .col(ColumnDef::new(Model::ChatId).integer().not_null())
            .col(ColumnDef::new(Model::MessageId).integer().not_null())
            .col(ColumnDef::new(Model::UserId).integer().not_null())
            .col(
                ColumnDef::new(Model::TypeId)
                    .string_len(50)
                    .not_null()
                    .default("message"),
            )
            .col(ColumnDef::new(Model::Text).text().not_null())
            .col(
                ColumnDef::new(Model::CreatedAt)
                    .timestamp_with_time_zone()
                    .not_null()
                    .default(Expr::current_timestamp()),
            )
            .col(
                ColumnDef::new(Model::UpdatedAt)
                    .timestamp_with_time_zone()
                    .not_null()
                    .default(Expr::current_timestamp()),
            )
            .primary_key(
                Index::create()
                    .name("message_pkey")
                    .col(Model::ChatId)
                    .col(Model::MessageId)
                    .primary(),
            )
            .foreign_key(
                ForeignKey::create()
                    .name("message_chat_id_fkey")
                    .from(Model::Table, Model::ChatId)
                    .to(Chat::Table, Chat::Id)
                    .on_delete(ForeignKeyAction::Cascade)
                    .on_update(ForeignKeyAction::Cascade),
            )
            .foreign_key(
                ForeignKey::create()
                    .name("message_user_id_fkey")
                    .from(Model::Table, Model::UserId)
                    .to(User::Table, User::Id)
                    .on_delete(ForeignKeyAction::Cascade)
                    .on_update(ForeignKeyAction::Cascade),
            )
            .foreign_key(
                ForeignKey::create()
                    .name("message_type_id_fkey")
                    .from(Model::Table, Model::TypeId)
                    .to(ModelType::Table, ModelType::Id)
                    .on_delete(ForeignKeyAction::Cascade)
                    .on_update(ForeignKeyAction::Cascade),
            )
            .clone();

        manager.create_table(table).await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let table: TableDropStatement = Table::drop().table(Model::Table).if_exists().clone();

        manager.drop_table(table).await?;

        let table: TableDropStatement = Table::drop().table(ModelType::Table).if_exists().clone();

        manager.drop_table(table).await
    }
}
