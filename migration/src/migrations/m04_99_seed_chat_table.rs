use sea_orm_migration::prelude::*;

#[derive(DeriveIden)]
pub enum Model {
    #[sea_orm(iden = "chat")]
    Table,
    IsPrivate,
}

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let insert: InsertStatement = Query::insert()
            .into_table(Model::Table)
            .columns([Model::IsPrivate])
            .values_panic([true.into()])
            .clone();

        manager.exec_stmt(insert).await
    }

    async fn down(&self, _: &SchemaManager) -> Result<(), DbErr> {
        Ok(())
    }
}
