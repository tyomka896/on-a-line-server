use dotenv::dotenv;
use sea_orm_migration::prelude::*;
use std::env;

mod database;

#[async_std::main]
async fn main() {
    dotenv().ok();

    env::set_var("DATABASE_URL", database::main_database_url());

    cli::run_cli(migration::Migrator).await;
}
