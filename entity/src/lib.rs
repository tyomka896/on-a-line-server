// #![allow(unused)]
mod entities;

pub use entities::chat;
pub use entities::message;
pub use entities::message_type;
pub use entities::participant;
pub use entities::token;
pub use entities::user;
pub use entities::user_has_chat;
pub use entities::user_has_chat_type;
pub use entities::verify;

#[cfg(test)]
mod entity_tests {
    use crate::{
        chat, message, message_type, participant, token, user, user_has_chat, user_has_chat_type,
        verify,
    };
    use sea_orm::{entity::prelude::*, DatabaseBackend, MockDatabase};

    #[async_std::test]
    async fn find_chat_by_id() -> Result<(), DbErr> {
        let chat: chat::Model = chat::Model {
            id: 1,
            ..chat::Model::default()
        };

        let db: DatabaseConnection = MockDatabase::new(DatabaseBackend::Postgres)
            .append_query_results([vec![chat.clone()]])
            .into_connection();

        assert_eq!(
            chat::Entity::find_by_id(1).one(&db).await?.unwrap().id,
            chat.id,
        );

        Ok(())
    }

    #[async_std::test]
    async fn chat_with_relations() -> Result<(), DbErr> {
        let chat: chat::Model = chat::Model {
            id: 1,
            ..chat::Model::default()
        };

        let message: message::Model = message::Model {
            chat_id: chat.id,
            message_id: 1,
            text: "Message".to_owned(),
            ..message::Model::default()
        };

        let participant: participant::Model = participant::Model {
            chat_id: chat.id,
            ..participant::Model::default()
        };

        let db: DatabaseConnection = MockDatabase::new(DatabaseBackend::Postgres)
            .append_query_results([vec![(chat.clone(), message.clone())]])
            .append_query_results([vec![(chat.clone(), participant.clone())]])
            .into_connection();

        assert_eq!(
            chat::Entity::find_by_id(1)
                .find_with_related(message::Entity)
                .all(&db)
                .await?,
            vec![(chat.clone(), vec![message])],
            "Message not found for chat."
        );

        assert_eq!(
            chat::Entity::find_by_id(1)
                .find_with_related(participant::Entity)
                .all(&db)
                .await?,
            vec![(chat.clone(), vec![participant])],
            "Participant not found for chat."
        );

        Ok(())
    }

    #[async_std::test]
    async fn find_message_by_id() -> Result<(), DbErr> {
        let message: message::Model = message::Model {
            chat_id: 1,
            message_id: 1,
            ..message::Model::default()
        };

        let db: DatabaseConnection = MockDatabase::new(DatabaseBackend::Postgres)
            .append_query_results([vec![message.clone()]])
            .into_connection();

        assert_eq!(
            message::Entity::find_by_id((1, 1)).one(&db).await?.unwrap(),
            message,
        );

        Ok(())
    }

    #[async_std::test]
    async fn message_with_relations() -> Result<(), DbErr> {
        let chat: chat::Model = chat::Model {
            id: 1,
            ..chat::Model::default()
        };

        let message_type: message_type::Model = message_type::Model {
            id: "message".to_owned(),
            ..message_type::Model::default()
        };

        let user: user::Model = user::Model {
            id: 1,
            ..user::Model::default()
        };

        let message: message::Model = message::Model {
            chat_id: chat.id,
            message_id: 1,
            user_id: user.id,
            type_id: message_type.id.to_owned(),
            ..message::Model::default()
        };

        let db: DatabaseConnection = MockDatabase::new(DatabaseBackend::Postgres)
            .append_query_results([vec![(message.clone(), chat.clone())]])
            .append_query_results([vec![(message.clone(), message_type.clone())]])
            .append_query_results([vec![(message.clone(), user.clone())]])
            .into_connection();

        assert_eq!(
            message::Entity::find_by_id((1, 1))
                .find_also_related(chat::Entity)
                .one(&db)
                .await?
                .unwrap(),
            (message.clone(), Some(chat)),
            "Chat not found for message."
        );

        assert_eq!(
            message::Entity::find_by_id((1, 1))
                .find_also_related(message_type::Entity)
                .one(&db)
                .await?
                .unwrap(),
            (message.clone(), Some(message_type)),
            "Message type not found for message."
        );

        assert_eq!(
            message::Entity::find_by_id((1, 1))
                .find_also_related(user::Entity)
                .one(&db)
                .await?
                .unwrap(),
            (message.clone(), Some(user)),
            "User not found for message."
        );

        Ok(())
    }

    #[async_std::test]
    async fn find_participant_by_id() -> Result<(), DbErr> {
        let participant: participant::Model = participant::Model {
            chat_id: 1,
            message_id: 1,
            user_id: 1,
            ..participant::Model::default()
        };

        let db: DatabaseConnection = MockDatabase::new(DatabaseBackend::Postgres)
            .append_query_results([vec![participant.clone()]])
            .into_connection();

        assert_eq!(
            participant::Entity::find_by_id((1, 1, 1))
                .one(&db)
                .await?
                .unwrap(),
            participant,
        );

        Ok(())
    }

    #[async_std::test]
    async fn participant_with_relations() -> Result<(), DbErr> {
        let chat: chat::Model = chat::Model {
            id: 1,
            ..chat::Model::default()
        };

        let user: user::Model = user::Model {
            id: 1,
            ..user::Model::default()
        };

        let message: message::Model = message::Model {
            chat_id: chat.id,
            message_id: 1,
            user_id: user.id,
            ..message::Model::default()
        };

        let participant: participant::Model = participant::Model {
            chat_id: chat.id,
            message_id: message.message_id,
            user_id: user.id,
            ..participant::Model::default()
        };

        let db: DatabaseConnection = MockDatabase::new(DatabaseBackend::Postgres)
            .append_query_results([vec![(participant.clone(), chat.clone())]])
            .append_query_results([vec![(participant.clone(), message.clone())]])
            .append_query_results([vec![(participant.clone(), user.clone())]])
            .into_connection();

        assert_eq!(
            participant::Entity::find_by_id((1, 1, 1))
                .find_also_related(chat::Entity)
                .one(&db)
                .await?
                .unwrap(),
            (participant.clone(), Some(chat)),
            "Chat not found for participant."
        );

        assert_eq!(
            participant::Entity::find_by_id((1, 1, 1))
                .find_also_related(message::Entity)
                .one(&db)
                .await?
                .unwrap(),
            (participant.clone(), Some(message)),
            "Message type not found for participant."
        );

        assert_eq!(
            participant::Entity::find_by_id((1, 1, 1))
                .find_also_related(user::Entity)
                .one(&db)
                .await?
                .unwrap(),
            (participant.clone(), Some(user)),
            "User not found for participant."
        );

        Ok(())
    }

    #[async_std::test]
    async fn find_user_by_id() -> Result<(), DbErr> {
        let user: user::Model = user::Model {
            id: 1,
            ..user::Model::default()
        };

        let db: DatabaseConnection = MockDatabase::new(DatabaseBackend::Postgres)
            .append_query_results([vec![user.clone()]])
            .into_connection();

        assert_eq!(
            user::Entity::find_by_id(1).one(&db).await?.unwrap().id,
            user.id,
        );

        Ok(())
    }

    #[async_std::test]
    async fn find_user_has_chat_by_id() -> Result<(), DbErr> {
        let user_has_chat: user_has_chat::Model = user_has_chat::Model {
            user_id: 1,
            chat_id: 1,
            ..user_has_chat::Model::default()
        };

        let db: DatabaseConnection = MockDatabase::new(DatabaseBackend::Postgres)
            .append_query_results([vec![user_has_chat.clone()]])
            .into_connection();

        assert_eq!(
            user_has_chat::Entity::find_by_id((1, 1))
                .one(&db)
                .await?
                .unwrap(),
            user_has_chat,
        );

        Ok(())
    }

    #[async_std::test]
    async fn user_has_chat_with_type() -> Result<(), DbErr> {
        let user_has_chat_type: user_has_chat_type::Model = user_has_chat_type::Model {
            id: "active".to_owned(),
            ..user_has_chat_type::Model::default()
        };

        let user_has_chat: user_has_chat::Model = user_has_chat::Model {
            user_id: 1,
            chat_id: 1,
            type_id: user_has_chat_type.id.to_owned(),
            ..user_has_chat::Model::default()
        };

        let db: DatabaseConnection = MockDatabase::new(DatabaseBackend::Postgres)
            .append_query_results([vec![(user_has_chat.clone(), user_has_chat_type.clone())]])
            .into_connection();

        assert_eq!(
            user_has_chat::Entity::find_by_id((1, 1))
                .find_also_related(user_has_chat_type::Entity)
                .one(&db)
                .await?
                .unwrap(),
            (user_has_chat, Some(user_has_chat_type)),
        );

        Ok(())
    }

    #[async_std::test]
    async fn find_token_by_id() -> Result<(), DbErr> {
        let token: token::Model = token::Model {
            id: 1,
            ..token::Model::default()
        };

        let db: DatabaseConnection = MockDatabase::new(DatabaseBackend::Postgres)
            .append_query_results([vec![token.clone()]])
            .into_connection();

        assert_eq!(
            token::Entity::find_by_id(1).one(&db).await?.unwrap().id,
            token.id,
        );

        Ok(())
    }

    #[async_std::test]
    async fn find_verify_by_id() -> Result<(), DbErr> {
        let verify: verify::Model = verify::Model {
            id: 1,
            ..verify::Model::default()
        };

        let db: DatabaseConnection = MockDatabase::new(DatabaseBackend::Postgres)
            .append_query_results([vec![verify.clone()]])
            .into_connection();

        assert_eq!(
            verify::Entity::find_by_id(1).one(&db).await?.unwrap().id,
            verify.id,
        );

        Ok(())
    }

    #[async_std::test]
    async fn user_with_relations() -> Result<(), DbErr> {
        let user: user::Model = user::Model {
            id: 1,
            ..user::Model::default()
        };

        let token: token::Model = token::Model {
            id: 1,
            user_id: user.id,
            ..token::Model::default()
        };

        let verify: verify::Model = verify::Model {
            id: 1,
            user_id: 1,
            ..verify::Model::default()
        };

        let db: DatabaseConnection = MockDatabase::new(DatabaseBackend::Postgres)
            .append_query_results([vec![(user.clone(), token.clone())]])
            .append_query_results([vec![(user.clone(), verify.clone())]])
            .into_connection();

        assert_eq!(
            user::Entity::find_by_id(1)
                .find_also_related(token::Entity)
                .one(&db)
                .await?
                .unwrap(),
            (user.clone(), Some(token)),
            "Token not found for user."
        );

        assert_eq!(
            user::Entity::find_by_id(1)
                .find_also_related(verify::Entity)
                .one(&db)
                .await?
                .unwrap(),
            (user.clone(), Some(verify)),
            "Verify not found for user."
        );

        Ok(())
    }
}
