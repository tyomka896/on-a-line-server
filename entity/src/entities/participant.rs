use sea_orm::{entity::prelude::*, prelude::async_trait::async_trait, Set};
use serde::{Deserialize, Serialize};

use super::chat::Entity as ChatEntity;
use super::message::Entity as MessageEntity;
use super::user::Entity as UserEntity;

#[derive(Debug, Clone, Default, PartialEq, DeriveEntityModel, Serialize, Deserialize)]
#[sea_orm(table_name = "participant")]
pub struct Model {
    #[sea_orm(primary_key)]
    pub chat_id: i32,
    #[sea_orm(primary_key)]
    pub message_id: i32,
    #[sea_orm(primary_key)]
    pub user_id: i32,
    #[sea_orm(default_value = false)]
    pub is_read: bool,
    pub created_at: DateTimeWithTimeZone,
    pub updated_at: DateTimeWithTimeZone,
    pub deleted_at: Option<DateTimeWithTimeZone>,
}

impl ActiveModel {
    pub fn from(chat_id: i32, message_id: i32, user_id: i32) -> Self {
        Self {
            chat_id: Set(chat_id),
            message_id: Set(message_id),
            user_id: Set(user_id),
            is_read: Set(false),
            ..Default::default()
        }
    }

    pub fn set_is_read(&mut self, value: bool) -> &mut Self {
        self.is_read = Set(value);

        self
    }
}

#[derive(Debug, Clone, EnumIter)]
pub enum Relation {
    ChatEntity,
    MessageEntity,
    UserEntity,
}

impl RelationTrait for Relation {
    fn def(&self) -> RelationDef {
        match self {
            Self::ChatEntity => Entity::belongs_to(ChatEntity)
                .from(Column::ChatId)
                .to(super::chat::Column::Id)
                .into(),
            Self::MessageEntity => Entity::belongs_to(MessageEntity)
                .from((Column::ChatId, Column::MessageId))
                .to((
                    super::message::Column::ChatId,
                    super::message::Column::MessageId,
                ))
                .into(),
            Self::UserEntity => Entity::belongs_to(UserEntity)
                .from(Column::UserId)
                .to(super::user::Column::Id)
                .into(),
        }
    }
}

impl Related<ChatEntity> for Entity {
    fn to() -> RelationDef {
        Relation::ChatEntity.def()
    }
}

impl Related<MessageEntity> for Entity {
    fn to() -> RelationDef {
        Relation::MessageEntity.def()
    }
}

impl Related<UserEntity> for Entity {
    fn to() -> RelationDef {
        Relation::UserEntity.def()
    }
}

#[async_trait]
impl ActiveModelBehavior for ActiveModel {
    async fn before_save<C>(mut self, _: &C, insert: bool) -> Result<Self, DbErr>
    where
        C: ConnectionTrait,
    {
        if !insert {
            self.updated_at = Set(chrono::Local::now().fixed_offset());
        }

        Ok(self)
    }
}
