use sea_orm::{entity::prelude::*, prelude::async_trait::async_trait, Set};
use serde::{Deserialize, Serialize};

use super::user::Entity as UserEntity;

#[derive(Debug, Default, Clone, PartialEq, DeriveEntityModel, Serialize, Deserialize)]
#[sea_orm(table_name = "token")]
pub struct Model {
    #[sea_orm(primary_key)]
    pub id: i32,
    pub user_id: i32,
    #[sea_orm(default_value = "auth")]
    pub name: String,
    pub token: String,
    pub last_used_at: Option<DateTimeWithTimeZone>,
    pub expires_at: Option<DateTimeWithTimeZone>,
    pub created_at: DateTimeWithTimeZone,
    pub updated_at: DateTimeWithTimeZone,
}

impl ActiveModel {
    pub fn from(user_id: i32, token: &str, name: &str) -> Self {
        Self {
            user_id: Set(user_id),
            token: Set(token.to_owned()),
            name: Set(name.to_owned()),
            ..Default::default()
        }
    }

    pub fn set_name(&mut self, value: &str) -> &mut Self {
        self.name = Set(value.to_owned());

        self
    }

    pub fn set_token(&mut self, value: &str) -> &mut Self {
        self.token = Set(value.to_owned());

        self
    }
}

#[derive(Debug, Clone, EnumIter)]
pub enum Relation {
    UserEntity,
}

impl RelationTrait for Relation {
    fn def(&self) -> RelationDef {
        match self {
            Self::UserEntity => Entity::belongs_to(UserEntity)
                .from(Column::UserId)
                .to(super::user::Column::Id)
                .into(),
        }
    }
}

impl Related<UserEntity> for Entity {
    fn to() -> RelationDef {
        Relation::UserEntity.def()
    }
}

#[async_trait]
impl ActiveModelBehavior for ActiveModel {
    async fn before_save<C>(mut self, _: &C, insert: bool) -> Result<Self, DbErr>
    where
        C: ConnectionTrait,
    {
        if !insert {
            self.updated_at = Set(chrono::Local::now().fixed_offset());
        }

        Ok(self)
    }
}
