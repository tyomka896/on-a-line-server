use sea_orm::{entity::prelude::*, prelude::async_trait::async_trait};
use serde::{Deserialize, Serialize};

use super::message::Entity as MessageEntity;

#[derive(Debug, Clone, Default, PartialEq, DeriveEntityModel, Serialize, Deserialize)]
#[sea_orm(table_name = "message_type")]
pub struct Model {
    #[sea_orm(primary_key)]
    pub id: String,
    pub about: Option<String>,
    pub created_at: DateTimeWithTimeZone,
}

impl ActiveModel {}

#[derive(Debug, Clone, EnumIter)]
pub enum Relation {
    MessageEntity,
}

impl RelationTrait for Relation {
    fn def(&self) -> RelationDef {
        match self {
            Self::MessageEntity => Entity::has_many(MessageEntity)
                .from(Column::Id)
                .to(super::message::Column::TypeId)
                .into(),
        }
    }
}

impl Related<MessageEntity> for Entity {
    fn to() -> RelationDef {
        Relation::MessageEntity.def()
    }
}

#[async_trait]
impl ActiveModelBehavior for ActiveModel {}
