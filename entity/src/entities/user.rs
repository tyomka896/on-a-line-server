use sea_orm::{entity::prelude::*, prelude::async_trait::async_trait, Set};
use serde::{Deserialize, Serialize};

use super::chat::Entity as ChatEntity;
use super::token::Entity as TokenEntity;
use super::verify::Entity as VerifyEntity;

#[derive(Debug, Clone, Default, PartialEq, DeriveEntityModel, Serialize, Deserialize)]
#[sea_orm(table_name = "user")]
pub struct Model {
    #[sea_orm(primary_key)]
    pub id: i32,
    #[sea_orm(unique)]
    pub username: String,
    pub first_name: Option<String>,
    pub last_name: Option<String>,
    pub email: Option<String>,
    #[serde(skip)]
    pub password: String,
    pub verified_at: Option<DateTimeWithTimeZone>,
    pub created_at: DateTimeWithTimeZone,
    pub updated_at: DateTimeWithTimeZone,
    pub last_visit: DateTimeWithTimeZone,
}

impl ActiveModel {
    pub fn from(username: &str, password: &str) -> Self {
        Self {
            username: Set(username.to_owned()),
            password: Set(password.to_owned()),
            ..Self::new()
        }
    }

    pub fn set_first_name(&mut self, value: &str) -> &mut Self {
        self.first_name = Set(if value.is_empty() {
            None
        } else {
            Some(value.to_owned())
        });

        self
    }

    pub fn set_last_name(&mut self, value: &str) -> &mut Self {
        self.last_name = Set(if value.is_empty() {
            None
        } else {
            Some(value.to_owned())
        });

        self
    }

    pub fn set_email(&mut self, value: &str) -> &mut Self {
        self.email = Set(if value.is_empty() {
            None
        } else {
            Some(value.to_owned())
        });

        self
    }

    pub fn set_password(&mut self, value: &str) -> &mut Self {
        self.password = Set(value.to_owned());

        self
    }
}

#[derive(Debug, Clone, EnumIter)]
pub enum Relation {
    TokenEntity,
    VerifyEntity,
}

impl RelationTrait for Relation {
    fn def(&self) -> RelationDef {
        match self {
            Self::TokenEntity => Entity::has_many(TokenEntity)
                .from(Column::Id)
                .to(super::token::Column::UserId)
                .into(),
            Self::VerifyEntity => Entity::has_many(VerifyEntity)
                .from(Column::Id)
                .to(super::verify::Column::UserId)
                .into(),
        }
    }
}

impl Related<ChatEntity> for Entity {
    fn to() -> RelationDef {
        super::user_has_chat::Relation::ChatEntity.def()
    }

    fn via() -> Option<RelationDef> {
        Some(super::user_has_chat::Relation::UserEntity.def().rev())
    }
}

impl Related<TokenEntity> for Entity {
    fn to() -> RelationDef {
        Relation::TokenEntity.def()
    }
}

impl Related<VerifyEntity> for Entity {
    fn to() -> RelationDef {
        Relation::VerifyEntity.def()
    }
}

#[async_trait]
impl ActiveModelBehavior for ActiveModel {
    async fn before_save<C>(mut self, _: &C, insert: bool) -> Result<Self, DbErr>
    where
        C: ConnectionTrait,
    {
        if !insert {
            self.updated_at = Set(chrono::Local::now().fixed_offset());
        }

        Ok(self)
    }
}
