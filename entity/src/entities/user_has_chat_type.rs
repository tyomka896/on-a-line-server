use sea_orm::{entity::prelude::*, prelude::async_trait::async_trait};
use serde::{Deserialize, Serialize};

use super::user_has_chat::Entity as UserHasChatEntity;

#[derive(Debug, Clone, Default, PartialEq, DeriveEntityModel, Serialize, Deserialize)]
#[sea_orm(table_name = "user_has_chat_type")]
pub struct Model {
    #[sea_orm(primary_key)]
    pub id: String,
    pub about: Option<String>,
    pub created_at: DateTimeWithTimeZone,
}

impl ActiveModel {}

#[derive(Debug, Clone, EnumIter)]
pub enum Relation {
    UserHasChatEntity,
}

impl RelationTrait for Relation {
    fn def(&self) -> RelationDef {
        match self {
            Self::UserHasChatEntity => Entity::has_many(UserHasChatEntity)
                .from(Column::Id)
                .to(super::user_has_chat::Column::TypeId)
                .into(),
        }
    }
}

impl Related<UserHasChatEntity> for Entity {
    fn to() -> RelationDef {
        Relation::UserHasChatEntity.def()
    }
}

#[async_trait]
impl ActiveModelBehavior for ActiveModel {}
