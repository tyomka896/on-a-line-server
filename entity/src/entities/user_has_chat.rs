use sea_orm::Set;
use sea_orm::{entity::prelude::*, prelude::async_trait::async_trait};
use serde::{Deserialize, Serialize};

use super::chat::Entity as ChatEntity;
use super::user::Entity as UserEntity;
use super::user_has_chat_type::Entity as UserHasChatTypeEntity;

#[derive(Debug, Clone, Default, PartialEq, DeriveEntityModel, Serialize, Deserialize)]
#[sea_orm(table_name = "user_has_chat")]
pub struct Model {
    #[sea_orm(primary_key)]
    pub user_id: i32,
    #[sea_orm(primary_key)]
    pub chat_id: i32,
    #[sea_orm(default_value = "active")]
    pub type_id: String,
    pub created_at: DateTimeWithTimeZone,
}

impl ActiveModel {
    pub fn from(user_id: i32, chat_id: i32) -> Self {
        Self {
            user_id: Set(user_id),
            chat_id: Set(chat_id),
            ..Default::default()
        }
    }
}

#[derive(Debug, Clone, EnumIter)]
pub enum Relation {
    ChatEntity,
    UserEntity,
    UserHasChatTypeEntity,
}

impl RelationTrait for Relation {
    fn def(&self) -> RelationDef {
        match self {
            Self::ChatEntity => Entity::belongs_to(ChatEntity)
                .from(Column::ChatId)
                .to(super::chat::Column::Id)
                .into(),
            Self::UserEntity => Entity::belongs_to(UserEntity)
                .from(Column::UserId)
                .to(super::user::Column::Id)
                .into(),
            Self::UserHasChatTypeEntity => Entity::belongs_to(UserHasChatTypeEntity)
                .from(Column::TypeId)
                .to(super::user_has_chat_type::Column::Id)
                .into(),
        }
    }
}

impl Related<ChatEntity> for Entity {
    fn to() -> RelationDef {
        Relation::ChatEntity.def()
    }
}

impl Related<UserEntity> for Entity {
    fn to() -> RelationDef {
        Relation::UserEntity.def()
    }
}

impl Related<UserHasChatTypeEntity> for Entity {
    fn to() -> RelationDef {
        Relation::UserHasChatTypeEntity.def()
    }
}

#[async_trait]
impl ActiveModelBehavior for ActiveModel {}
