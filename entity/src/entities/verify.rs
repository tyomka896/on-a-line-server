use sea_orm::{entity::prelude::*, prelude::async_trait::async_trait, Set};
use serde::{Deserialize, Serialize};

use super::user::Entity as UserEntity;

#[derive(Debug, Clone, Default, PartialEq, DeriveEntityModel, Serialize, Deserialize)]
#[sea_orm(table_name = "verify")]
pub struct Model {
    #[sea_orm(primary_key)]
    pub id: i32,
    pub user_id: i32,
    #[sea_orm(column_name = "type")]
    pub type_val: String,
    pub email: String,
    pub code: String,
    pub attempts: i32,
    pub is_valid: bool,
    pub created_at: DateTimeWithTimeZone,
    pub updated_at: DateTimeWithTimeZone,
}

impl ActiveModel {}

#[derive(Debug, Clone, EnumIter)]
pub enum Relation {
    UserEntity,
}

impl RelationTrait for Relation {
    fn def(&self) -> RelationDef {
        match self {
            Self::UserEntity => Entity::belongs_to(UserEntity)
                .from(Column::UserId)
                .to(super::user::Column::Id)
                .into(),
        }
    }
}

impl Related<UserEntity> for Entity {
    fn to() -> RelationDef {
        Relation::UserEntity.def()
    }
}

#[async_trait]
impl ActiveModelBehavior for ActiveModel {
    async fn before_save<C>(mut self, _: &C, insert: bool) -> Result<Self, DbErr>
    where
        C: ConnectionTrait,
    {
        if !insert {
            self.updated_at = Set(chrono::Local::now().fixed_offset());
        }

        Ok(self)
    }
}
