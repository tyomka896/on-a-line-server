use sea_orm::{entity::prelude::*, prelude::async_trait::async_trait, Set};
use serde::{Deserialize, Serialize};

use super::message::Entity as MessageEntity;
use super::participant::Entity as ParticipantEntity;
use super::user::Entity as UserEntity;
use super::user_has_chat::Entity as UserHasChatEntity;

#[derive(Debug, Clone, Default, PartialEq, DeriveEntityModel, Serialize, Deserialize)]
#[sea_orm(table_name = "chat")]
pub struct Model {
    #[sea_orm(primary_key)]
    pub id: i32,
    pub name: Option<String>,
    #[sea_orm(default_value = true)]
    pub is_private: bool,
    pub created_at: DateTimeWithTimeZone,
    pub updated_at: DateTimeWithTimeZone,
}

impl ActiveModel {
    fn from(name: Option<&str>, is_private: bool) -> Self {
        let name: Option<String> = match name {
            Some(value) => {
                let value: String = value.replace("‎", "");

                Some(value.trim().to_owned())
            }
            None => None,
        };

        Self {
            name: Set(name),
            is_private: Set(is_private),
            ..Default::default()
        }
    }

    pub fn new_private() -> Self {
        Self::from(None, true)
    }

    pub fn new_public(name: &str) -> Self {
        Self::from(Some(name), false)
    }
}

#[derive(Debug, Clone, EnumIter)]
pub enum Relation {
    MessageEntity,
    ParticipantEntity,
    UserHasChatEntity,
}

impl RelationTrait for Relation {
    fn def(&self) -> RelationDef {
        match self {
            Self::MessageEntity => Entity::has_many(MessageEntity)
                .from(Column::Id)
                .to(super::message::Column::ChatId)
                .into(),
            Self::ParticipantEntity => Entity::has_many(ParticipantEntity)
                .from(Column::Id)
                .to(super::participant::Column::ChatId)
                .into(),
            Self::UserHasChatEntity => Entity::has_many(UserHasChatEntity)
                .from(Column::Id)
                .to(super::user_has_chat::Column::ChatId)
                .into(),
        }
    }
}

impl Related<MessageEntity> for Entity {
    fn to() -> RelationDef {
        Relation::MessageEntity.def()
    }
}

impl Related<ParticipantEntity> for Entity {
    fn to() -> RelationDef {
        Relation::ParticipantEntity.def()
    }
}

impl Related<UserEntity> for Entity {
    fn to() -> RelationDef {
        super::user_has_chat::Relation::UserEntity.def()
    }

    fn via() -> Option<RelationDef> {
        Some(super::user_has_chat::Relation::ChatEntity.def().rev())
    }
}

impl Related<UserHasChatEntity> for Entity {
    fn to() -> RelationDef {
        Relation::UserHasChatEntity.def()
    }
}

#[async_trait]
impl ActiveModelBehavior for ActiveModel {
    async fn before_save<C>(mut self, _: &C, insert: bool) -> Result<Self, DbErr>
    where
        C: ConnectionTrait,
    {
        if !insert {
            self.updated_at = Set(chrono::Local::now().fixed_offset());
        }

        Ok(self)
    }
}
