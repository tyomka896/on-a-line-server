use sea_orm::{entity::prelude::*, prelude::async_trait::async_trait, Set};
use serde::{Deserialize, Serialize};

use super::chat::Entity as ChatEntity;
use super::message_type::Entity as MessageTypeEntity;
use super::participant::Entity as ParticipantEntity;
use super::user::Entity as UserEntity;

#[derive(Debug, Clone, Default, PartialEq, DeriveEntityModel, Serialize, Deserialize)]
#[sea_orm(table_name = "message")]
pub struct Model {
    #[sea_orm(primary_key)]
    pub chat_id: i32,
    #[sea_orm(primary_key)]
    pub message_id: i32,
    pub user_id: i32,
    #[sea_orm(default_value = "message")]
    pub type_id: String,
    pub text: String,
    #[sea_orm(ignore)]
    pub is_read: bool,
    pub created_at: DateTimeWithTimeZone,
    pub updated_at: DateTimeWithTimeZone,
}

impl ActiveModel {
    pub fn from(chat_id: i32, user_id: i32, text: &str) -> Self {
        let text: String = text.replace("‎", "");

        Self {
            chat_id: Set(chat_id),
            user_id: Set(user_id),
            text: Set(text.trim().to_owned()),
            ..Default::default()
        }
    }
}

#[derive(Debug, Clone, EnumIter)]
pub enum Relation {
    ChatEntity,
    MessageTypeEntity,
    ParticipantEntity,
    UserEntity,
}

impl RelationTrait for Relation {
    fn def(&self) -> RelationDef {
        match self {
            Self::ChatEntity => Entity::belongs_to(ChatEntity)
                .from(Column::ChatId)
                .to(super::chat::Column::Id)
                .into(),
            Self::MessageTypeEntity => Entity::belongs_to(MessageTypeEntity)
                .from(Column::TypeId)
                .to(super::message_type::Column::Id)
                .into(),
            Self::ParticipantEntity => Entity::belongs_to(ParticipantEntity)
                .from((Column::ChatId, Column::MessageId))
                .to((
                    super::participant::Column::ChatId,
                    super::participant::Column::MessageId,
                ))
                .into(),
            Self::UserEntity => Entity::belongs_to(UserEntity)
                .from(Column::UserId)
                .to(super::user::Column::Id)
                .into(),
        }
    }
}

impl Related<ChatEntity> for Entity {
    fn to() -> RelationDef {
        Relation::ChatEntity.def()
    }
}

impl Related<MessageTypeEntity> for Entity {
    fn to() -> RelationDef {
        Relation::MessageTypeEntity.def()
    }
}

impl Related<ParticipantEntity> for Entity {
    fn to() -> RelationDef {
        Relation::ParticipantEntity.def()
    }
}

impl Related<UserEntity> for Entity {
    fn to() -> RelationDef {
        Relation::UserEntity.def()
    }
}

#[async_trait]
impl ActiveModelBehavior for ActiveModel {
    async fn before_save<C>(mut self, _: &C, insert: bool) -> Result<Self, DbErr>
    where
        C: ConnectionTrait,
    {
        if !insert {
            self.updated_at = Set(chrono::Local::now().fixed_offset());
        }

        Ok(self)
    }
}
