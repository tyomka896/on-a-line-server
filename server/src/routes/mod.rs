use actix_web::{web, Scope};

use crate::http::{
    controllers::api::{auth, chat, message, token, user},
    middleware,
};

pub fn api_routes() -> Scope {
    web::scope("/api")
        .service(auth_routes())
        .service(chat_routes())
        .service(chats_routes())
        .service(message_routes())
        .service(token_routes())
        .service(user_routes())
        .service(users_routes())
}

pub fn auth_routes() -> Scope {
    web::scope("/auth").service(
        web::scope("")
            .wrap(middleware::Auth)
            .route("", web::get().to(auth::info))
            .route("", web::put().to(auth::update))
            .route("/password", web::put().to(auth::password)),
    )
}

pub fn chat_routes() -> Scope {
    web::scope("/chat").service(
        web::scope("")
            .wrap(middleware::Auth)
            .route("/private", web::post().to(chat::create_private))
            .route("/public", web::post().to(chat::create_public))
            .route("/{id}/add-user/{user_id}", web::put().to(chat::add_user))
            .route("/{id}/messages", web::post().to(chat::model_messages))
            .route("/{id}/users", web::post().to(chat::model_users)),
    )
}

pub fn chats_routes() -> Scope {
    web::scope("/chats").service(
        web::scope("")
            .wrap(middleware::Auth)
            .route("", web::post().to(chat::models))
            .route("/to-read", web::post().to(chat::models_to_read)),
    )
}

pub fn message_routes() -> Scope {
    web::scope("/message").service(
        web::scope("")
            .wrap(middleware::Auth)
            .route("", web::post().to(message::create))
            .route("/{id}", web::put().to(message::update))
            .route("/{id}", web::delete().to(message::delete)),
    )
}

pub fn token_routes() -> Scope {
    web::scope("/token")
        .route("/login", web::post().to(token::login))
        .route("/auth", web::get().to(token::auth))
        .route("/revoke", web::delete().to(token::revoke))
        .service(
            web::scope("")
                .wrap(middleware::Auth)
                .route("/check", web::get().to(token::check)),
        )
}

pub fn user_routes() -> Scope {
    web::scope("/user").service(
        web::scope("")
            .wrap(middleware::Auth)
            .route("", web::post().to(user::create))
            .route("/exists", web::post().to(user::exists))
            .route("/{id}", web::get().to(user::model))
            .route("/{id}", web::put().to(user::update))
            .route("/{id}", web::delete().to(user::delete)),
    )
}

pub fn users_routes() -> Scope {
    web::scope("/users").service(
        web::scope("")
            .wrap(middleware::Auth)
            .route("", web::post().to(user::models)),
    )
}
