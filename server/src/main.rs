#![allow(unused)]
use actix_web::{
    middleware::{Logger, NormalizePath},
    web, App, HttpServer,
};
use dotenv::dotenv;

mod config;
mod http;
mod routes;
mod utils;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    dotenv().ok();

    env_logger::builder()
        .format_target(false)
        .filter(None, config::app::debug_mode_level())
        .init();

    let address: &str = "localhost:3000";

    let main_state: web::Data<http::data::MainState> = http::data::MainState::init().await;

    let app_entry = move || {
        App::new()
            .app_data(main_state.clone())
            .configure(http::data::configure)
            .wrap(Logger::default())
            .wrap(NormalizePath::trim())
            .service(routes::api_routes())
    };

    let http_server = HttpServer::new(app_entry)
        .workers(workers_len())
        .bind(address)?
        .run();

    log::info!("Server started on {}!", address);

    http_server.await
}

fn workers_len() -> usize {
    let available: usize =
        std::thread::available_parallelism().map_or(2, std::num::NonZeroUsize::get);

    if cfg!(debug_assertions) {
        1
    } else {
        available
    }
}
