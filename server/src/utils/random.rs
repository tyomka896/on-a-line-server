use rand::Rng;
use std::time::{SystemTime, UNIX_EPOCH};

const ENCODING: &'static str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

/// Random float number between 0 and 1
pub fn random() -> f64 {
    rand::thread_rng().gen()
}

/// Random between min and max value
pub fn random_in(min: i64, max: i64) -> f64 {
    let mut min = min as f64;
    let mut max = max as f64;

    if min > max {
        std::mem::swap(&mut min, &mut max);
    }

    random() * (max - min) + min
}

/// Random string with specified length
pub fn random_str(length: u64) -> String {
    let mut str: String = String::new();

    let chars: Vec<char> = ENCODING.chars().collect::<Vec<char>>();
    let chars_len: i64 = chars.len() as i64;

    for _ in 0..length {
        str.push(chars[random_in(0, chars_len).floor() as usize])
    }

    str
}

/// Simplified ulid generation
pub fn unique() -> String {
    let mut str: String = String::new();

    let chars: Vec<char> = ENCODING.chars().collect::<Vec<char>>();
    let chars_len: u128 = chars.len() as u128;

    let mut time: u128 = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_millis();
    let mut time_mod: u128;

    while time > 0 {
        time_mod = time % chars_len;

        str.push(chars[time_mod as usize]);

        time = (time - time_mod) / chars_len;
    }

    str = str.chars().rev().collect::<String>();

    str.push_str(random_str(4).as_str());

    str
}
