use crate::utils::random;

/// Personal token with plain and hash values
pub struct PersonalToken {
    plain: String,
    hashed: String,
}

impl PersonalToken {
    pub fn new() -> Self {
        let plain_value: String = random::unique();

        Self {
            plain: plain_value.to_owned(),
            hashed: sha256::digest(&plain_value),
        }
    }

    pub fn plain_value(&self) -> String {
        self.plain.to_owned()
    }

    pub fn hashed_value(&self) -> String {
        self.hashed.to_owned()
    }
}
