use chrono::{DateTime, FixedOffset};
use sea_orm::entity::*;

mod personal_token;

use entity::token;
pub use personal_token::PersonalToken;

/// Default token name
pub const DEFAULT_TOKEN_NAME: &'static str = "auth";

/// Token api to create a new token for the user
pub struct TokenApi {
    model: token::ActiveModel,
    token: PersonalToken,
}

impl TokenApi {
    pub fn from(user_id: i32, name: Option<String>) -> Self {
        let token: PersonalToken = PersonalToken::new();

        let model: token::ActiveModel = token::ActiveModel::from(
            user_id,
            &token.hashed_value(),
            &name.unwrap_or(DEFAULT_TOKEN_NAME.to_owned()),
        );

        Self { model, token }
    }

    /// Set token expires date
    pub fn set_expires_at(&mut self, expires_at: DateTime<FixedOffset>) -> &mut Self {
        self.model.expires_at = ActiveValue::Set(Some(expires_at));

        self
    }

    /// Return token model info
    pub fn model(&self) -> token::ActiveModel {
        self.model.clone()
    }

    /// Plain token value
    pub fn plain_token_value(&self) -> String {
        self.token.plain_value()
    }
}
