use std::env;

pub fn env_def(key: &str, def: &str) -> String {
    match env::var(key) {
        Ok(value) => {
            if value.is_empty() {
                def.to_owned()
            } else {
                value.trim().to_owned()
            }
        }
        Err(_) => def.to_owned(),
    }
}
