use super::env::env_def;

pub fn debug_mode() -> String {
    env_def("DEBUG_MODE", "debug")
}

pub fn debug_mode_level() -> log::LevelFilter {
    match debug_mode().as_str() {
        "info" => log::LevelFilter::Info,
        "warn" => log::LevelFilter::Warn,
        "error" => log::LevelFilter::Error,
        _ => log::LevelFilter::Debug,
    }
}
