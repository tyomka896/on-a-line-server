use super::env::env_def;

#[derive(Default)]
struct Database {
    pub protocol: String,
    pub host: String,
    pub port: String,
    pub database: String,
    pub username: String,
    pub password: String,
}

impl Database {
    fn database_url(self) -> String {
        format!(
            "{protocol}://{username}:{password}@{host}:{port}/{database}",
            protocol = self.protocol,
            host = self.host,
            port = self.port,
            database = self.database,
            username = self.username,
            password = self.password,
        )
    }
}

pub fn main_database_url() -> String {
    let mut config: Database = Database::default();

    config.protocol = env_def("DB_PROTOCOL", "postgres");
    config.host = env_def("DB_HOST", "localhost");
    config.port = env_def("DB_PORT", "5432");
    config.database = env_def("DB_DATABASE", "postgres");
    config.username = env_def("DB_USERNAME", "postgres");
    config.password = env_def("DB_PASSWORD", "postgres");

    config.database_url()
}
