use serde::Serialize;
use std::collections::HashMap;

#[derive(Debug, Clone, Serialize)]
pub struct HttpMessage {
    pub message: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub errors: Option<HashMap<String, Vec<String>>>,
}

impl HttpMessage {
    pub fn from(message: &str) -> Self {
        Self {
            message: message.to_owned(),
            errors: None,
        }
    }

    /// Custom message of the HTTP status error
    pub fn set_message(&mut self, message: &str) -> &mut Self {
        self.message = message.to_owned();

        self
    }

    /// Add new error to list by key and value
    pub fn add_error(&mut self, key: &str, value: &str) -> &mut Self {
        let errors = match &mut self.errors {
            Some(value) => value,
            None => {
                self.errors =
                    Some(HashMap::from([(key.to_owned(), vec![value.to_owned()])]));

                return self;
            }
        };

        errors
            .entry(key.to_owned())
            .and_modify(|list| list.push(value.to_owned()))
            .or_insert(vec![value.to_owned()]);

        self
    }
}

impl std::fmt::Display for HttpMessage {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut message: String = self.message.to_owned();

        if let Some(errors) = &self.errors {
            let text: String = errors
                .into_iter()
                .map(|(key, values)| format!("- {}: {}", key, values.join("; ")))
                .collect::<Vec<String>>()
                .join("\n");

            message.push_str(format!("\n{text}").as_str());
        }

        write!(f, "{}", message)
    }
}
