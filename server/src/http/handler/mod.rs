use actix_web::{
    http::{header::ContentType, StatusCode},
    HttpResponse, ResponseError,
};
use sea_orm::DbErr;
use validator::{ValidationErrors, ValidationErrorsKind};

mod http_message;

use http_message::HttpMessage;

#[derive(Debug, Clone)]
pub enum HttpContentType {
    JSON,
    PLAINTEXT,
}

impl HttpContentType {
    pub fn header(&self) -> ContentType {
        match self {
            HttpContentType::JSON => ContentType::json(),
            HttpContentType::PLAINTEXT => ContentType::plaintext(),
        }
    }
}

#[derive(Debug, Clone)]
pub struct HttpError {
    http_message: HttpMessage,
    status_code: StatusCode,
    content_type: HttpContentType,
}

impl HttpError {
    fn from(status_code: StatusCode) -> Self {
        Self {
            http_message: HttpMessage::from(status_code.to_string().as_str()),
            status_code,
            content_type: HttpContentType::JSON,
        }
    }

    /// HTTP Status Code - Bad Request
    pub fn bad_request() -> Self {
        Self::from(StatusCode::BAD_REQUEST)
    }

    /// HTTP Status Code - Unauthorized
    pub fn unauthorized() -> Self {
        Self::from(StatusCode::UNAUTHORIZED)
    }

    /// HTTP Status Code - Forbidden
    pub fn forbidden() -> Self {
        Self::from(StatusCode::FORBIDDEN)
    }

    /// HTTP Status Code - Not Found
    pub fn not_found() -> Self {
        Self::from(StatusCode::NOT_FOUND)
    }

    /// HTTP Status Code - Unprocessable Entity
    pub fn unprocessable() -> Self {
        Self::from(StatusCode::UNPROCESSABLE_ENTITY)
    }

    /// HTTP Status Code - Unprocessable Entity
    pub fn internal_error() -> Self {
        Self::from(StatusCode::INTERNAL_SERVER_ERROR)
    }

    /// HTTP Status Code - Unprocessable Entity
    pub fn not_implemented() -> Self {
        Self::from(StatusCode::NOT_IMPLEMENTED)
    }

    /// Description of the error
    pub fn set_description(mut self, message: &str) -> Self {
        self.http_message.message = format!("{}: {}", self.status_code.to_string(), message);

        self
    }

    /// Custom message of the HTTP status error
    pub fn set_message(mut self, message: &str) -> Self {
        self.http_message.set_message(message);

        self
    }

    /// Content type
    pub fn set_content_type(mut self, content_type: HttpContentType) -> Self {
        self.content_type = content_type;

        self
    }

    /// Add new error to list by key and value
    pub fn add_error(&mut self, key: &str, value: &str) -> &mut Self {
        self.http_message.add_error(key, value);

        self
    }
}

impl std::fmt::Display for HttpError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.http_message.to_string())
    }
}

impl From<Box<dyn std::error::Error + 'static>> for HttpError {
    fn from(value: Box<dyn std::error::Error>) -> Self {
        HttpError::bad_request().set_description(value.to_string().as_str())
    }
}

fn validate_errors(http_error: &mut HttpError, key: &str, kind: &ValidationErrorsKind) {
    match kind {
        ValidationErrorsKind::Field(errors) => {
            errors.iter().for_each(|elem| {
                http_error.add_error(key, &elem.to_string());
            });
        }
        ValidationErrorsKind::Struct(errors) => {
            Box::clone(errors).into_errors().iter().for_each(|elem| {
                validate_errors(http_error, elem.0, elem.1);
            });
        }
        _ => (),
    }
}

impl From<ValidationErrors> for HttpError {
    fn from(value: ValidationErrors) -> Self {
        let mut http_error: HttpError = HttpError::unprocessable();

        for (key, kind) in value.0.iter() {
            validate_errors(&mut http_error, key, kind);
        }

        http_error
    }
}

impl From<DbErr> for HttpError {
    fn from(value: DbErr) -> Self {
        HttpError::bad_request().set_description(value.to_string().as_str())
    }
}

impl ResponseError for HttpError {
    fn error_response(&self) -> HttpResponse<actix_web::body::BoxBody> {
        let mut http_builder = HttpResponse::build(self.status_code());

        http_builder.insert_header(self.content_type.header());

        match self.content_type {
            HttpContentType::JSON => http_builder.json(self.http_message.clone()),
            HttpContentType::PLAINTEXT => http_builder.body(self.http_message.to_string()),
        }
    }

    fn status_code(&self) -> StatusCode {
        self.status_code
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::collections::HashMap;

    #[test]
    fn bad_request_message() {
        let http_error: HttpError = HttpError::bad_request();

        assert_eq!(
            http_error.http_message.message,
            "400 Bad Request".to_owned(),
        );
    }

    #[test]
    fn unauthorized_message() {
        let http_error: HttpError = HttpError::unauthorized();

        assert_eq!(
            http_error.http_message.message,
            "401 Unauthorized".to_owned(),
        );
    }

    #[test]
    fn not_found_message() {
        let http_error: HttpError = HttpError::not_found();

        assert_eq!(http_error.http_message.message, "404 Not Found".to_owned(),);
    }

    #[test]
    fn unprocessable_message() {
        let http_error: HttpError = HttpError::unprocessable();

        assert_eq!(
            http_error.http_message.message,
            "422 Unprocessable Entity".to_owned(),
        );
    }

    #[test]
    fn internal_error_message() {
        let http_error: HttpError = HttpError::internal_error();

        assert_eq!(
            http_error.http_message.message,
            "500 Internal Server Error".to_owned(),
        );
    }

    #[test]
    fn set_description_message() {
        let http_error: HttpError = HttpError::bad_request().set_description("Description");

        assert_eq!(
            http_error.http_message.message,
            "400 Bad Request: Description".to_owned(),
        );
    }

    #[test]
    fn set_custom_message() {
        let http_error: HttpError = HttpError::bad_request().set_message("Custom message");

        assert_eq!(http_error.http_message.message, "Custom message".to_owned());
    }

    #[test]
    fn error_messages() {
        let mut http_error: HttpError = HttpError::unprocessable();

        http_error.add_error("key", "error");
        http_error.add_error("key", "message");

        let result: HashMap<String, Vec<String>> = HashMap::<String, Vec<String>>::from([(
            "key".to_owned(),
            vec!["error".to_string(), "message".to_string()],
        )]);

        assert_eq!(http_error.http_message.errors, Some(result));
    }

    #[test]
    fn as_plaintext() {
        let mut http_error: HttpError =
            HttpError::bad_request().set_content_type(HttpContentType::PLAINTEXT);

        http_error.add_error("key", "error");
        http_error.add_error("key", "message");

        assert_eq!(
            http_error.to_string(),
            "400 Bad Request\n- key: error; message".to_owned()
        );
    }
}
