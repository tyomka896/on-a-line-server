use actix_web::{web, HttpResponse};
use sea_orm::{entity::*, query::*};
use validator::Validate;

use crate::http::requests::{
    auth::AuthInfo,
    user::{CreateRequest, ExistsRequest, ModelsRequest, UpdateRequest},
};
use crate::http::{data::MainState, handler::HttpError};
use entity::user;

pub async fn create(
    _: AuthInfo,
    state: web::Data<MainState>,
    message: web::Form<CreateRequest>,
) -> Result<HttpResponse, HttpError> {
    message.validate()?;

    let username_exists = user::Entity::find()
        .filter(user::Column::Username.eq(&message.username))
        .one(&state.db)
        .await?;

    if username_exists.is_some() {
        return Err(HttpError::unprocessable()
            .add_error("username", "The username has already been taken.")
            .clone());
    }

    let password_hashed: String = bcrypt::hash(&message.password, bcrypt::DEFAULT_COST).unwrap();

    let mut created: user::ActiveModel =
        user::ActiveModel::from(&message.username, &password_hashed);

    if let Some(value) = &message.first_name {
        created.set_first_name(value);
    }

    if let Some(value) = &message.last_name {
        created.set_last_name(value);
    }

    if let Some(value) = &message.email {
        created.set_email(value);
    }

    Ok(HttpResponse::Ok().json(created.insert(&state.db).await?))
}

pub async fn exists(
    _: AuthInfo,
    state: web::Data<MainState>,
    message: web::Form<ExistsRequest>,
) -> Result<HttpResponse, HttpError> {
    let user_info = user::Entity::find()
        .filter(user::Column::Username.eq(&message.username))
        .one(&state.db)
        .await?;

    match user_info {
        Some(_) => Ok(HttpResponse::Ok().json(true)),
        None => Ok(HttpResponse::Ok().json(false)),
    }
}

pub async fn model(
    _: AuthInfo,
    state: web::Data<MainState>,
    path: web::Path<i32>,
) -> Result<HttpResponse, HttpError> {
    let id: i32 = path.into_inner();

    let user_info: user::Model = user::Entity::find()
        .filter(user::Column::Id.eq(id))
        .one(&state.db)
        .await?
        .ok_or(
            HttpError::unprocessable()
                .set_description(format!("The user with id #{id} does not exists.").as_str()),
        )?;

    Ok(HttpResponse::Ok().json(user_info))
}

pub async fn update(
    _: AuthInfo,
    state: web::Data<MainState>,
    path: web::Path<i32>,
    message: web::Form<UpdateRequest>,
) -> Result<HttpResponse, HttpError> {
    let id: i32 = path.into_inner();

    let user_info: user::Model = user::Entity::find()
        .filter(user::Column::Id.eq(id))
        .one(&state.db)
        .await?
        .ok_or(
            HttpError::unprocessable()
                .set_description(format!("The user with id #{id} does not exists.").as_str()),
        )?;

    message.validate()?;

    let mut updated: user::ActiveModel = user_info.clone().into();

    if let Some(value) = &message.first_name {
        updated.set_first_name(value);
    }

    if let Some(value) = &message.last_name {
        updated.set_last_name(value);
    }

    if let Some(value) = &message.email {
        updated.set_email(value);
    }

    Ok(HttpResponse::Ok().json(updated.save(&state.db).await?.try_into_model()?))
}

pub async fn delete(
    _: AuthInfo,
    state: web::Data<MainState>,
    path: web::Path<i32>,
) -> Result<HttpResponse, HttpError> {
    let id: i32 = path.into_inner();

    let _: user::Model = user::Entity::find()
        .filter(user::Column::Id.eq(id))
        .one(&state.db)
        .await?
        .ok_or(
            HttpError::unprocessable()
                .set_description(format!("The user with id #{id} does not exists.").as_str()),
        )?;

    // TODO: Should be roles to define who can delete a user
    Err(HttpError::not_implemented())

    // let result: sea_orm::DeleteResult = value.delete(&state.db).await?;
    // Ok(HttpResponse::Ok().json(true))
}

pub async fn models(
    _: AuthInfo,
    state: web::Data<MainState>,
    message: web::Query<ModelsRequest>,
) -> Result<HttpResponse, HttpError> {
    message.validate()?;

    let per_page: u64 = message.per_page.unwrap_or(10) as u64;

    let users_vec: Vec<user::Model> = user::Entity::find()
        .filter(user::Column::Username.contains(&message.username))
        .order_by_asc(user::Column::Username)
        .order_by_asc(user::Column::LastName)
        .order_by_asc(user::Column::FirstName)
        .limit(per_page)
        .all(&state.db)
        .await?;

    Ok(HttpResponse::Ok().json(serde_json::json!({
        "data": users_vec,
        "per_page": per_page,
    })))
}
