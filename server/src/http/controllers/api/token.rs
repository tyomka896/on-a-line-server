use actix_web::{web, HttpResponse};
use sea_orm::{entity::*, query::*};
use validator::Validate;

use crate::http::{
    data::MainState,
    handler::HttpError,
    requests::auth::AuthInfo,
    requests::token::{AuthRequest, LoginRequest, RevokeRequest},
};
use crate::utils::token_api::TokenApi;
use entity::{token, user};

pub async fn login(
    state: web::Data<MainState>,
    message: web::Form<LoginRequest>,
) -> Result<HttpResponse, HttpError> {
    message.validate()?;

    let auth: user::Model = user::Entity::find()
        .filter(user::Column::Username.contains(&message.login))
        .one(&state.db)
        .await?
        .ok_or(
            HttpError::unprocessable()
                .add_error("login", "The username is not found.")
                .clone(),
        )?;

    if !bcrypt::verify(&message.password, &auth.password).unwrap() {
        return Err(HttpError::unprocessable()
            .add_error("password", "The password is not correct.")
            .clone());
    }

    let personal_token: TokenApi = TokenApi::from(auth.id, message.name.clone());

    personal_token.model().insert(&state.db).await?;

    let response = serde_json::json!({"token": personal_token.plain_token_value()});

    Ok(HttpResponse::Ok().json(response))
}

pub async fn auth(
    state: web::Data<MainState>,
    message: web::Query<AuthRequest>,
) -> Result<HttpResponse, HttpError> {
    if let Some(value) = &message.token {
        let (_, user_info): (token::Model, Option<user::Model>) = token::Entity::find()
            .filter(token::Column::Token.eq(sha256::digest(value)))
            .find_also_related(user::Entity)
            .one(&state.db)
            .await?
            .ok_or(
                HttpError::unprocessable()
                    .add_error("token", "Token is not found.")
                    .clone(),
            )?;

        if let Some(value) = user_info {
            return Ok(HttpResponse::Ok().json(value));
        } else {
            return Err(HttpError::unprocessable().set_description("The user is not found."));
        }
    } else if message.login.clone().is_some() && message.password.is_some() {
        let login: String = message.login.as_ref().unwrap().to_owned();
        let password: String = message.password.as_ref().unwrap().to_owned();

        let user_info: user::Model = user::Entity::find()
            .filter(user::Column::Username.eq(login))
            .one(&state.db)
            .await?
            .ok_or(HttpError::unprocessable().set_description("The user is not found."))?;

        if bcrypt::verify(&password, &user_info.password).unwrap() {
            return Ok(HttpResponse::Ok().json(user_info));
        }
    }

    Err(HttpError::unauthorized())
}

pub async fn revoke(
    state: web::Data<MainState>,
    message: web::Query<RevokeRequest>,
) -> Result<HttpResponse, HttpError> {
    let auth: user::Model = user::Entity::find()
        .filter(user::Column::Username.eq(&message.login))
        .one(&state.db)
        .await?
        .ok_or(HttpError::unprocessable().set_description("The user is not found."))?;

    if !bcrypt::verify(&message.password, &auth.password).unwrap() {
        return Err(HttpError::unauthorized());
    }

    let mut find_tokens = token::Entity::find().filter(token::Column::UserId.eq(auth.id));

    if let Some(value) = &message.name {
        find_tokens = find_tokens.filter(token::Column::Name.eq(value));
    }

    let tokens_id: Vec<i32> = find_tokens
        .all(&state.db)
        .await?
        .iter()
        .map(|elem| elem.id)
        .collect::<Vec<i32>>();

    let result: sea_orm::DeleteResult = token::Entity::delete_many()
        .filter(token::Column::Id.is_in(tokens_id))
        .exec(&state.db)
        .await?;

    let response = serde_json::json!({"revoked": result.rows_affected});

    Ok(HttpResponse::Ok().json(response))
}

pub async fn check(auth: AuthInfo) -> Result<HttpResponse, HttpError> {
    let message: String = format!("Hello, {}!", auth.username);

    Ok(HttpResponse::Ok().json(serde_json::json!({"message": message})))
}
