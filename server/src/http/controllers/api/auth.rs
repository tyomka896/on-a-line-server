use actix_web::{web, HttpResponse};
use sea_orm::entity::*;
use validator::Validate;

use crate::http::requests::auth::{AuthInfo, PasswordRequest, UpdateRequest};
use crate::http::{data::MainState, handler::HttpError};
use entity::user;

pub async fn info(auth: AuthInfo) -> Result<HttpResponse, HttpError> {
    Ok(HttpResponse::Ok().json(auth.clone()))
}

pub async fn update(
    auth: AuthInfo,
    state: web::Data<MainState>,
    message: web::Form<UpdateRequest>,
) -> Result<HttpResponse, HttpError> {
    message.validate()?;

    let mut updated: user::ActiveModel = auth.clone().into();

    if let Some(value) = &message.first_name {
        updated.set_first_name(value);
    }

    if let Some(value) = &message.last_name {
        updated.set_last_name(value);
    }

    if let Some(value) = &message.email {
        updated.set_email(value);
    }

    Ok(HttpResponse::Ok().json(updated.save(&state.db).await?.try_into_model()?))
}

pub async fn password(
    auth: AuthInfo,
    state: web::Data<MainState>,
    message: web::Form<PasswordRequest>,
) -> Result<HttpResponse, HttpError> {
    message.validate()?;

    if !bcrypt::verify(&message.current, &auth.password).unwrap() {
        return Err(HttpError::unprocessable()
            .add_error("current", "Current password is not correct.")
            .clone());
    }

    if &message.current == &message.password {
        return Err(HttpError::unprocessable()
            .add_error(
                "password",
                "The current and new password current must be different.",
            )
            .clone());
    }

    let mut updated: user::ActiveModel = auth.clone().into();

    let password_hashed: String = bcrypt::hash(&message.password, bcrypt::DEFAULT_COST).unwrap();

    updated.set_password(&password_hashed);

    updated.save(&state.db).await?;

    Ok(HttpResponse::Ok().json(serde_json::json!({"message": "Your password has been reset."})))
}
