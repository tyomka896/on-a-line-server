use actix_web::{web, HttpResponse};
use sea_orm::{entity::*, query::*};

use crate::http::{
    data::MainState,
    handler::HttpError,
    requests::{auth::AuthInfo, message::CreateRequest},
};
use entity::{chat, message, participant, user_has_chat};

pub async fn create(
    auth: AuthInfo,
    state: web::Data<MainState>,
    message: web::Form<CreateRequest>,
) -> Result<HttpResponse, HttpError> {
    let chat_id: i32 = message.chat_id;

    let text: String = message.text.to_owned().replace("‎", "");
    let text: &str = text.trim();

    if text.is_empty() {
        return Err(HttpError::unprocessable()
            .add_error("text", "The text may not be empty string.")
            .clone());
    }

    let (chat_info, _) = chat::Entity::find()
        .find_also_related(user_has_chat::Entity)
        .filter(chat::Column::Id.eq(chat_id))
        .filter(user_has_chat::Column::UserId.eq(auth.id))
        .one(&state.db)
        .await?
        .ok_or(
            HttpError::unprocessable()
                .add_error(
                    "chat_id",
                    "The selected chat is invalid or must be a participant.",
                )
                .clone(),
        )?;

    let user_has_chat_vec: Vec<user_has_chat::Model> = chat_info
        .find_related(user_has_chat::Entity)
        .all(&state.db)
        .await?;

    let message_info: message::Model = message::ActiveModel::from(chat_info.id, auth.id, &text)
        .insert(&state.db)
        .await?;

    let mut participants_vec: Vec<participant::ActiveModel> = vec![];

    for user_has_chat_info in user_has_chat_vec {
        let mut participant_info: participant::ActiveModel = participant::ActiveModel::from(
            chat_info.id,
            message_info.message_id,
            user_has_chat_info.user_id,
        );

        if auth.id == user_has_chat_info.user_id {
            participant_info.set_is_read(true);
        }

        participants_vec.push(participant_info);
    }

    participant::Entity::insert_many(participants_vec)
        .exec(&state.db)
        .await?;

    Ok(HttpResponse::Ok().json(message_info))
}

pub async fn update() -> Result<HttpResponse, HttpError> {
    Err(HttpError::not_implemented())
}

pub async fn delete() -> Result<HttpResponse, HttpError> {
    Err(HttpError::not_implemented())
}
