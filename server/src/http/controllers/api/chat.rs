use actix_web::{web, HttpResponse};
use sea_orm::{prelude::*, query::*, sea_query::*, FromQueryResult};
use serde::Serialize;
use std::collections::HashMap;
use validator::Validate;

use crate::http::{
    data::MainState,
    handler::HttpError,
    requests::{
        auth::AuthInfo,
        chat::{CreatePrivateRequest, CreatePublicRequest, ModelMessagesRequest, ModelsRequest},
    },
};
use entity::{chat, message, participant, user, user_has_chat};

pub async fn create_private(
    auth: AuthInfo,
    state: web::Data<MainState>,
    message: web::Query<CreatePrivateRequest>,
) -> Result<HttpResponse, HttpError> {
    message.validate()?;

    if auth.id == message.with_user_id {
        return Err(HttpError::unprocessable()
            .add_error("with_user_id", "Unable to create chat with yourself (yet).")
            .clone());
    }

    let with_user_info_select: Option<user::Model> = user::Entity::find()
        .filter(user::Column::Id.eq(message.with_user_id))
        .one(&state.db)
        .await?;

    if with_user_info_select.is_none() {
        return Err(HttpError::unprocessable()
            .add_error("with_user_id", "The selected with_user_id is invalid.")
            .clone());
    };

    #[derive(FromQueryResult)]
    struct Count {
        count: i64,
    }
    #[derive(FromQueryResult)]
    struct Empty {}

    let chats_between: Vec<(Count, Option<Empty>)> = chat::Entity::find()
        .find_also_related(user_has_chat::Entity)
        .select_only()
        .column_as(Expr::col(user_has_chat::Column::ChatId).count(), "A_count")
        .filter(chat::Column::IsPrivate.eq(true))
        .filter(user_has_chat::Column::UserId.is_in([auth.id, message.with_user_id]))
        .group_by(user_has_chat::Column::ChatId)
        .into_model::<Count, Empty>()
        .all(&state.db)
        .await?;

    if chats_between.iter().any(|(elem, _)| elem.count == 2) {
        return Err(HttpError::unprocessable()
            .add_error(
                "with_user_id",
                "The chat with selected user already exists.",
            )
            .clone());
    }

    let chat_info: chat::Model = chat::ActiveModel::new_private().insert(&state.db).await?;

    user_has_chat::Entity::insert_many([
        user_has_chat::ActiveModel::from(auth.id, chat_info.id),
        user_has_chat::ActiveModel::from(message.with_user_id, chat_info.id),
    ])
    .exec(&state.db)
    .await?;

    Ok(HttpResponse::Ok().json(chat_info))
}

pub async fn create_public(
    auth: AuthInfo,
    state: web::Data<MainState>,
    message: web::Query<CreatePublicRequest>,
) -> Result<HttpResponse, HttpError> {
    message.validate()?;

    let name: String = message.name.to_owned().replace("‎", "");
    let name: &str = name.trim();

    if name.is_empty() {
        return Err(HttpError::unprocessable()
            .add_error("name", "The name may not be empty string.")
            .clone());
    }

    let chat_info: chat::Model = chat::ActiveModel::new_public(name)
        .insert(&state.db)
        .await?;

    user_has_chat::Entity::insert(user_has_chat::ActiveModel::from(auth.id, chat_info.id))
        .exec(&state.db)
        .await?;

    Ok(HttpResponse::Ok().json(chat_info))
}

pub async fn add_user(
    auth: AuthInfo,
    state: web::Data<MainState>,
    info: web::Path<(i32, i32)>,
) -> Result<HttpResponse, HttpError> {
    let (chat_id, user_id) = info.into_inner();

    let (chat_info, _) = chat::Entity::find()
        .find_also_related(user_has_chat::Entity)
        .filter(chat::Column::Id.eq(chat_id))
        .filter(user_has_chat::Column::UserId.eq(auth.id))
        .one(&state.db)
        .await?
        .ok_or(
            HttpError::unprocessable()
                .add_error(
                    "chat_id",
                    "The selected chat is invalid or must be a participant.",
                )
                .clone(),
        )?;

    if chat_info.is_private {
        return Err(HttpError::unprocessable()
            .add_error(
                "chat_id",
                "The selected chat is private, adding user only to group chat allowed.",
            )
            .clone());
    }

    if auth.id == user_id {
        return Err(HttpError::unprocessable()
            .add_error("user_id", "The selected user already added in the chat.")
            .clone());
    }

    let user_info_select: Option<user::Model> = user::Entity::find()
        .filter(user::Column::Id.eq(user_id))
        .one(&state.db)
        .await?;

    if user_info_select.is_none() {
        return Err(HttpError::unprocessable()
            .add_error("user_id", "The selected user_id is invalid.")
            .clone());
    };

    let already_in_chat: u64 = user_has_chat::Entity::find()
        .filter(user_has_chat::Column::UserId.eq(user_id))
        .filter(user_has_chat::Column::ChatId.eq(chat_id))
        .count(&state.db)
        .await?;

    if already_in_chat >= 1 {
        return Err(HttpError::unprocessable()
            .add_error("user_id", "The selected user is already is the chat.")
            .clone());
    }

    user_has_chat::Entity::insert(user_has_chat::ActiveModel::from(user_id, chat_id))
        .exec(&state.db)
        .await?;

    Ok(HttpResponse::Ok().json(true))
}

pub async fn model_messages(
    auth: AuthInfo,
    state: web::Data<MainState>,
    info: web::Path<i32>,
    message: web::Query<ModelMessagesRequest>,
) -> Result<HttpResponse, HttpError> {
    message.validate()?;

    let chat_id: i32 = info.into_inner();

    let from_created_at: chrono::DateTime<chrono::Utc> = message
        .from_created_at
        .unwrap_or(chrono::Local::now().to_utc());

    let per_page: u64 = message.per_page.unwrap_or(15) as u64;

    let (chat_info, _) = chat::Entity::find()
        .find_also_related(user_has_chat::Entity)
        .filter(chat::Column::Id.eq(chat_id))
        .filter(user_has_chat::Column::UserId.eq(auth.id))
        .one(&state.db)
        .await?
        .ok_or(
            HttpError::unprocessable()
                .add_error(
                    "chat_id",
                    "The selected chat is invalid or must be a participant.",
                )
                .clone(),
        )?;

    let mut messages_vec: Vec<message::Model> = message::Entity::find()
        .filter(message::Column::ChatId.eq(chat_info.id))
        .filter(message::Column::CreatedAt.lt(from_created_at))
        .order_by_desc(message::Column::CreatedAt)
        .limit(per_page)
        .all(&state.db)
        .await?;

    #[derive(FromQueryResult)]
    struct Count {
        message_id: i32,
        count: i64,
    }

    let participants_vec: Vec<Count> = participant::Entity::find()
        .select_only()
        .column(participant::Column::MessageId)
        .expr_as(
            Func::sum(Expr::col(participant::Column::IsRead).cast_as(Alias::new("int"))),
            "count",
        )
        .clone()
        .filter(participant::Column::ChatId.eq(chat_info.id))
        .filter(
            participant::Column::MessageId.is_in(messages_vec.iter().map(|elem| elem.message_id)),
        )
        .filter(participant::Column::UserId.ne(auth.id))
        .group_by(participant::Column::MessageId)
        .order_by_asc(participant::Column::MessageId)
        .into_model::<Count>()
        .all(&state.db)
        .await?;

    let read_statuses: HashMap<&i32, bool> =
        participants_vec
            .iter()
            .fold(HashMap::new(), |mut acc, elem| {
                acc.insert(&elem.message_id, elem.count > 0);

                acc
            });

    let response: Vec<&mut message::Model> = messages_vec
        .iter_mut()
        .map(|elem| {
            elem.is_read = read_statuses
                .get(&elem.message_id)
                .unwrap_or(&false)
                .to_owned();

            elem
        })
        .collect();

    Ok(HttpResponse::Ok().json(response))
}

pub async fn model_users(
    auth: AuthInfo,
    state: web::Data<MainState>,
    info: web::Path<i32>,
) -> Result<HttpResponse, HttpError> {
    let chat_id: i32 = info.into_inner();

    let (chat_info, _) = chat::Entity::find()
        .find_also_related(user_has_chat::Entity)
        .filter(chat::Column::Id.eq(chat_id))
        .filter(user_has_chat::Column::UserId.eq(auth.id))
        .one(&state.db)
        .await?
        .ok_or(
            HttpError::unprocessable()
                .add_error(
                    "chat_id",
                    "The selected chat is invalid or must be a participant.",
                )
                .clone(),
        )?;

    let response: Vec<user::Model> = chat_info.find_related(user::Entity).all(&state.db).await?;

    Ok(HttpResponse::Ok().json(response))
}

pub async fn models(
    auth: AuthInfo,
    state: web::Data<MainState>,
    message: web::Query<ModelsRequest>,
) -> Result<HttpResponse, HttpError> {
    message.validate()?;

    let page_count: u64 = message.page_count.unwrap_or(1) as u64;

    let per_page: u64 = message.per_page.unwrap_or(50) as u64;

    let total: u64 = chat::Entity::find()
        .find_also_related(user_has_chat::Entity)
        .filter(user_has_chat::Column::UserId.eq(auth.id))
        .count(&state.db)
        .await?;

    let mut private_chats_id: Vec<i32> = vec![];

    let mut user_chats_vec: Vec<chat::Model> = chat::Entity::find()
        .find_also_related(user_has_chat::Entity)
        .filter(user_has_chat::Column::UserId.eq(auth.id))
        .order_by_desc(chat::Column::CreatedAt)
        .limit(per_page)
        .offset(per_page * page_count - per_page)
        .all(&state.db)
        .await?
        .iter()
        .map(|(elem, _)| {
            if elem.is_private {
                private_chats_id.push(elem.id);
            }

            elem.clone()
        })
        .collect();

    let users_info_vec: HashMap<i32, String> = user_has_chat::Entity::find()
        .find_also_related(user::Entity)
        .filter(user_has_chat::Column::UserId.ne(auth.id))
        .filter(user_has_chat::Column::ChatId.is_in(private_chats_id))
        .all(&state.db)
        .await?
        .iter()
        .fold(HashMap::new(), |mut acc, (left, right)| {
            if let Some(value) = right {
                acc.insert(left.chat_id, value.username.to_owned());
            }

            acc
        });

    let chats_vec: Vec<chat::Model> = user_chats_vec
        .iter_mut()
        .map(|elem| {
            if elem.is_private {
                elem.name = users_info_vec.get(&elem.id).cloned()
            }

            elem.clone()
        })
        .collect();

    Ok(HttpResponse::Ok().json(serde_json::json!({
        "data": chats_vec,
        "per_page": per_page,
        "page_count": page_count,
        "total": total,
    })))
}

pub async fn models_to_read(
    auth: AuthInfo,
    state: web::Data<MainState>,
) -> Result<HttpResponse, HttpError> {
    #[derive(Serialize, FromQueryResult)]
    struct Count {
        chat_id: i32,
        last_message: Option<String>,
        count: i64,
    }

    let mut counts_vec: Vec<Count> = participant::Entity::find()
        .select_only()
        .column(participant::Column::ChatId)
        .expr(Func::count(Expr::col(participant::Column::ChatId)))
        .clone()
        .filter(participant::Column::UserId.eq(auth.id))
        .filter(participant::Column::IsRead.eq(false))
        .filter(participant::Column::DeletedAt.is_null())
        .group_by(participant::Column::ChatId)
        .into_model::<Count>()
        .all(&state.db)
        .await?;

    let chats_id: Vec<i32> = counts_vec.iter().map(|elem| elem.chat_id).collect();

    let last_messages_vec: Vec<message::Model> = message::Entity::find()
        .distinct_on([(message::Entity, message::Column::ChatId)])
        .filter(message::Column::ChatId.is_in(chats_id))
        .order_by_desc(message::Column::ChatId)
        .order_by_desc(message::Column::CreatedAt)
        .all(&state.db)
        .await?;

    let last_messages: HashMap<i32, String> =
        last_messages_vec
            .iter()
            .fold(HashMap::new(), |mut acc, elem| {
                acc.insert(elem.chat_id, elem.text.to_owned());

                acc
            });

    let response: Vec<&mut Count> = counts_vec
        .iter_mut()
        .map(|elem| {
            elem.last_message = last_messages.get(&elem.chat_id).cloned();

            elem
        })
        .collect();

    Ok(HttpResponse::Ok().json(response))
}
