use actix_web::{HttpRequest, HttpResponse};

use crate::http::handler::HttpError;

/// API route Not Found response
pub async fn not_found(_req: HttpRequest) -> Result<HttpResponse, HttpError> {
    Err(HttpError::not_found())
}
