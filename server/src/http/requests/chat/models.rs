use serde::Deserialize;
use validator::Validate;

#[derive(Debug, Deserialize, Validate)]
pub struct ModelsRequest {
    #[validate(range(
        min = 1,
        max = 100,
        message = "The per_page must be between 1 and 100."
    ))]
    pub per_page: Option<i64>,
    #[validate(range(
        min = 1,
        max = 1000,
        message = "The page_count must be between 1 and 10000."
    ))]
    pub page_count: Option<i64>,
}
