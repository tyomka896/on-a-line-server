use chrono::{DateTime, Utc};
use serde::Deserialize;
use serde_with::formats::Flexible;
use serde_with::TimestampMilliSeconds;
use validator::Validate;

#[serde_with::serde_as]
#[derive(Debug, Deserialize, Validate)]
pub struct ModelMessagesRequest {
    #[serde_as(as = "Option<TimestampMilliSeconds<String, Flexible>>")]
    pub from_created_at: Option<DateTime<Utc>>,
    #[validate(range(
        min = 1,
        max = 100,
        message = "The per_page must be between 1 and 100."
    ))]
    pub per_page: Option<i64>,
}
