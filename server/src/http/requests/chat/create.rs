use serde::Deserialize;
use validator::Validate;

#[derive(Debug, Deserialize, Validate)]
pub struct CreatePublicRequest {
    #[validate(length(
        min = 3,
        max = 100,
        message = "The name must be between 3 and 100 characters."
    ))]
    pub name: String,
}

#[derive(Debug, Deserialize, Validate)]
pub struct CreatePrivateRequest {
    pub with_user_id: i32,
}
