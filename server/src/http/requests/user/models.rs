use serde::Deserialize;
use validator::Validate;

#[derive(Debug, Deserialize, Validate)]
pub struct ModelsRequest {
    #[validate(length(
        min = 1,
        max = 100,
        message = "The username must be between 1 and 100 characters."
    ))]
    pub username: String,
    #[validate(range(min = 1, max = 25, message = "The per_page must be between 1 and 25."))]
    pub per_page: Option<i64>,
}
