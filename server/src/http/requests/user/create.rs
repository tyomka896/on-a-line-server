use serde::Deserialize;
use validator::Validate;

#[derive(Debug, Deserialize, Validate)]
pub struct CreateRequest {
    #[validate(length(
        min = 3,
        max = 50,
        message = "The username must be between 3 and 50 characters."
    ))]
    pub username: String,
    #[validate(length(
        max = 100,
        message = "The first_name may not be greater than 100 characters."
    ))]
    pub first_name: Option<String>,
    #[validate(length(
        max = 100,
        message = "The last_name may not be greater than 100 characters."
    ))]
    pub last_name: Option<String>,
    #[validate(
        email(message = "The email must be a valid email address."),
        length(
            max = 100,
            message = "The email may not be greater than 100 characters."
        )
    )]
    pub email: Option<String>,
    #[validate(length(
        min = 6,
        max = 60,
        message = "Passwords must be at least six characters and match the confirmation."
    ))]
    pub password: String,
    #[validate(must_match(other = "password", message = "The confirmation does not match."))]
    pub password_confirmation: String,
}
