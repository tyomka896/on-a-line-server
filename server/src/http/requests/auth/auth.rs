use actix_web::{FromRequest, HttpMessage};
use std::future;

use crate::http::handler::HttpError;
use entity::user;

pub struct AuthInfo(user::Model);

impl FromRequest for AuthInfo {
    type Error = actix_web::Error;

    type Future = future::Ready<Result<Self, Self::Error>>;

    fn from_request(
        req: &actix_web::HttpRequest,
        _: &mut actix_web::dev::Payload,
    ) -> Self::Future {
        let auth: Option<user::Model> = req.extensions().get::<user::Model>().cloned();

        let result = match auth {
            Some(value) => Ok(AuthInfo(value)),
            None => Err(HttpError::unauthorized().into()),
        };

        future::ready(result)
    }
}

impl std::ops::Deref for AuthInfo {
    type Target = user::Model;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
