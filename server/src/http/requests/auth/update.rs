use serde::Deserialize;
use validator::Validate;

#[derive(Debug, Deserialize, Validate)]
pub struct UpdateRequest {
    #[validate(length(
        max = 100,
        message = "The first_name may not be greater than 100 characters."
    ))]
    pub first_name: Option<String>,
    #[validate(length(
        max = 100,
        message = "The last_name may not be greater than 100 characters."
    ))]
    pub last_name: Option<String>,
    #[validate(
        email(message = "The email must be a valid email address."),
        length(
            max = 100,
            message = "The email may not be greater than 100 characters."
        )
    )]
    pub email: Option<String>,
}
