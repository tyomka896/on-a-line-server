use serde::Deserialize;
use validator::Validate;

#[derive(Debug, Deserialize, Validate)]
pub struct CreateRequest {
    pub chat_id: i32,
    #[validate(length(
        min = 1,
        max = 2048,
        message = "The text must be between 1 and 2048 characters."
    ))]
    pub text: String,
}