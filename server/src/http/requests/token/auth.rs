use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct AuthRequest {
    pub token: Option<String>,
    pub login: Option<String>,
    pub password: Option<String>,
}
