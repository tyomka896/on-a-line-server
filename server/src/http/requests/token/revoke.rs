use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct RevokeRequest {
    pub login: String,
    pub password: String,
    pub name: Option<String>,
}
