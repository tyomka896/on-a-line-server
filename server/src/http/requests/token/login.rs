use serde::Deserialize;
use validator::Validate;

#[derive(Debug, Deserialize, Validate)]
pub struct LoginRequest {
    pub login: String,
    pub password: String,
    #[validate(length(
        min = 4,
        max = 100,
        message = "The name must be between 4 and 100 characters."
    ))]
    pub name: Option<String>,
}
