use actix_web::web;

use crate::http::handler::HttpError;

pub fn config_validation(cfg: &mut web::ServiceConfig) {
    cfg.app_data(form_validate())
    .app_data(json_validate())
    .app_data(path_validate())
    .app_data(query_validate());
}

fn form_validate() -> web::FormConfig {
    web::FormConfig::default().error_handler(|error, _| {
        log::debug!("FormConfig: {:?}", error.to_string().as_str());

        HttpError::unprocessable()
            .add_error("form", error.to_string().as_str())
            .clone()
            .into()
    })
}

fn json_validate() -> web::JsonConfig {
    web::JsonConfig::default().error_handler(|error, _| {
        log::debug!("JsonConfig: {}", error.to_string().as_str());

        HttpError::unprocessable()
            .add_error("json", error.to_string().as_str())
            .clone()
            .into()
    })
}

fn path_validate() -> web::PathConfig {
    web::PathConfig::default().error_handler(|error, _| {
        log::debug!("PathConfig: {:?}", error.to_string().as_str());

        HttpError::unprocessable()
            .add_error("path", error.to_string().as_str())
            .clone()
            .into()
    })
}

fn query_validate() -> web::QueryConfig {
    web::QueryConfig::default().error_handler(|error, _| {
        log::debug!("QueryConfig: {:?}", error.to_string().as_str());

        HttpError::unprocessable()
            .add_error("query", error.to_string().as_str())
            .clone()
            .into()
    })
}
