use actix_web::web;
use sea_orm::{ConnectOptions, Database, DatabaseConnection};

use crate::config::{app, database};

pub struct MainState {
    pub db: DatabaseConnection,
}

impl MainState {
    pub async fn init() -> web::Data<Self> {
        let mut db_options: ConnectOptions = ConnectOptions::new(database::main_database_url());

        db_options
            .sqlx_logging(true)
            .sqlx_logging_level(app::debug_mode_level())
            .set_schema_search_path("public");

        web::Data::new(Self {
            db: Database::connect(db_options)
                .await
                .expect("Unable to connect to database."),
        })
    }
}
