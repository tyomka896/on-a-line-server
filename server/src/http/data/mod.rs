use actix_web::web;

mod state;
mod validation;

use crate::http::controllers;
pub use state::MainState;

pub fn configure(cfg: &mut web::ServiceConfig) {
    cfg.configure(validation::config_validation)
        .default_service(web::route().to(controllers::default::not_found));
}
