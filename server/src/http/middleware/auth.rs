use actix_web::{
    dev::{forward_ready, Service, ServiceRequest, ServiceResponse, Transform},
    error::{ErrorBadRequest, ErrorUnauthorized},
    http, web, Error, HttpMessage,
};
use entity::{token, user};
use futures_util::{future::LocalBoxFuture, FutureExt};
use sea_orm::{ColumnTrait, ConnectionTrait, EntityTrait, QueryFilter, Statement};
use std::{future, rc::Rc};

use crate::http::{data::MainState, handler::HttpError};

pub struct Auth;

impl<S> Transform<S, ServiceRequest> for Auth
where
    S: Service<
            ServiceRequest,
            Response = ServiceResponse<actix_web::body::BoxBody>,
            Error = actix_web::Error,
        > + 'static,
{
    type Response = ServiceResponse<actix_web::body::BoxBody>;
    type Error = Error;
    type InitError = ();
    type Transform = AuthMiddleware<S>;
    type Future = future::Ready<Result<Self::Transform, Self::InitError>>;

    fn new_transform(&self, service: S) -> Self::Future {
        future::ready(Ok(AuthMiddleware {
            service: Rc::new(service),
        }))
    }
}

pub struct AuthMiddleware<S> {
    service: Rc<S>,
}

impl<S> Service<ServiceRequest> for AuthMiddleware<S>
where
    S: Service<ServiceRequest, Response = ServiceResponse<actix_web::body::BoxBody>, Error = Error>
        + 'static,
{
    type Response = ServiceResponse<actix_web::body::BoxBody>;
    type Error = Error;
    type Future = LocalBoxFuture<'static, Result<Self::Response, Self::Error>>;

    forward_ready!(service);

    fn call(&self, req: ServiceRequest) -> Self::Future {
        let header: Option<String> = req
            .headers()
            .get(http::header::AUTHORIZATION)
            .map(|elem| elem.to_str().unwrap().to_owned());

        let Some(header_value) = header else {
            return future::ready(Err(HttpError::unauthorized().into())).boxed_local();
        };

        if !header_value.starts_with("Bearer ") {
            return future::ready(Err(HttpError::unauthorized().into())).boxed_local();
        }

        let token_value: String = header_value.replace("Bearer ", "");

        let state: web::Data<MainState> = req.app_data::<web::Data<MainState>>().unwrap().clone();

        let srv: Rc<S> = Rc::clone(&self.service);

        async move {
            let (_, auth) = token::Entity::find()
                .filter(token::Column::Token.eq(sha256::digest(token_value)))
                .find_also_related(user::Entity)
                .one(&state.db)
                .await
                .map_err(|_| ErrorBadRequest(HttpError::bad_request()))?
                .ok_or(ErrorUnauthorized(HttpError::unauthorized()))?;

            if let Some(value) = auth {
                let _ = &state
                    .db
                    .execute(Statement::from_sql_and_values(
                        sea_orm::DatabaseBackend::Postgres,
                        "UPDATE \"user\" SET \"last_visit\"=$1 WHERE \"id\"=$2",
                        [chrono::Local::now().fixed_offset().into(), value.id.into()],
                    ))
                    .await;

                req.extensions_mut().insert::<user::Model>(value);
            }

            Ok(srv.call(req).await?)
        }
        .boxed_local()
    }
}
